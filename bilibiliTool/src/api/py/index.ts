import Request from '../../utils/request'

const url = "http://127.0.0.1:8000";

export const getLocalInfo = () => Request({
  url: url + '/getLocalInfo/',
  method: 'get',
})

export const getUserInfo = (mid: any) => Request({
  url: url + '/getUserInfo/',
  method: 'get',
  params: {
    mid
  }
})

export const saveUserInfo = (params: any) => Request({
  url: url + '/saveUserInfo/',
  method: 'get',
  params
})

export const getUserStat = (vmid: any) => Request({
  url: url + '/getUserStat/',
  method: 'get',
  params:{
    vmid
  }
})

export const logout = () => Request({
  url: url + '/logout/',
  method: 'get',
})

export const getDynamicInfo = (dynamic_id:number|string, offset:number|string) => Request({
  url: url + '/getDynamicInfo/',
  method: 'get',
  params:{
    dynamic_id,
    offset
  }
})
