const isNotBlank = (cs: any) => {
    if(Object.is(null, cs)) return false;
    if(Object.is(undefined, cs)) return false;
    var arr = Object.keys(cs);
    var count = 0;
    for (var key in cs) {
        if (!cs[key]) count++;
    }
    if (arr.length == count) return false;
    return true;
}

export default {
    isNotBlank,
}