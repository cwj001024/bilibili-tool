import axios from 'axios'
import router from '../router'

// 创建axios的实例
const service = axios.create({
    // baseURL: 'http://localhost:8000/',
    timeout: 2500,
})
// 请求拦截器，携带token字段
service.interceptors.request.use(
    config => {
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

// 响应拦截器
service.interceptors.response.use(
    response => {
        const res = response.data
        return res
    }
)

export default service