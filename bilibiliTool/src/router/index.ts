import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('../views/index.vue')
    },
    {
      path: '/index',
      component: () => import('../views/index.vue')
    },
    {
      path: '/dynamic',
      name: 'dynamic',
      component: () => import('../views/dynamic.vue')
    },
    {
      path: '/choujiangRes',
      name: 'luckDreaw',
      component: () => import('../views/luckDraw.vue')
    },
  ]
})

export default router
